import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SharedGlobalProvider } from '../shared-global/shared-global';

/*
  Generated class for the SurveyserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SurveyserviceProvider {

  constructor(public http: HttpClient,public globalVar:SharedGlobalProvider) {}

  getSurvey(){
    return this.http.get(this.globalVar.baseUrl+'surveys?uid='+this.globalVar.getUserId(),
    {headers:new HttpHeaders().set('token',this.globalVar.getToken())}
    );
  }

  getSurveyDetail(uid:number){
    return this.http.get(this.globalVar.baseUrl+'survey-single-detail/'+uid,
    {headers:new HttpHeaders().set('token',this.globalVar.getToken())}
    );
  }

  editSurveyDetail(uid:number,sno:number){
    return this.http.get(this.globalVar.baseUrl+'view-user-submission/'+uid+'/'+sno,
    {headers:new HttpHeaders().set('token',this.globalVar.getToken())}
    );
  }

  postSurvey(values:any,surveyId:number,uid:number){
    return this.http.post(this.globalVar.baseUrl+'survey-submit/'+surveyId+'/'+uid+'/0',values,
    {headers:new HttpHeaders().set('token',this.globalVar.getToken())}  
    );
  }

  editPostSurvey(values:any,surveyId:number,uid:number,sno:number){
    return this.http.post(this.globalVar.baseUrl+'survey-submit/'+surveyId+'/'+uid+'/'+sno,values,
    {headers:new HttpHeaders().set('token',this.globalVar.getToken())}  
    );
  }

  getuserSurveys(userId:number){
    return this.http.get(this.globalVar.baseUrl+'user-submissions/'+userId,
    {headers:new HttpHeaders().set('token',this.globalVar.getToken())}  
    );
  }
}
