import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SharedGlobalProvider } from '../shared-global/shared-global';

/*
  Generated class for the UserserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserserviceProvider {

  constructor(public http: HttpClient,public globalVar:SharedGlobalProvider) {}

  registerUser(userArr:any){
    return this.http.post(this.globalVar.baseUrl+'auth/register',userArr);
  }

  loginUser(userArr:any){
    return this.http.post(this.globalVar.baseUrl+'auth/login',userArr);
  }

  forgotUser(userArr:any){
    return this.http.post(this.globalVar.baseUrl+'forgot-password',userArr);
  }

  changePassword(userArr:any){
    return this.http.post(this.globalVar.baseUrl+'change-password/'+this.globalVar.getUserId(),userArr);
  }
}
