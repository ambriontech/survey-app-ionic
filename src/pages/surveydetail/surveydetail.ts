import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { SurveyserviceProvider } from '../../providers/surveyservice/surveyservice';
import { SurveyModel } from '../../shared/survey.model';
import { NgForm } from '@angular/forms';
import { SharedGlobalProvider } from '../../providers/shared-global/shared-global';
import { Storage } from '@ionic/storage';
import { SubmissionlistPage } from '../submissionlist/submissionlist';
import { LoginPage } from '../login/login';
import { ThankyouPage } from '../thankyou/thankyou';

@IonicPage()
@Component({
  selector: 'page-surveydetail',
  templateUrl: 'surveydetail.html'
})
export class SurveydetailPage {
  surveyQuestion=[];
  surveyItemDetail:SurveyModel=new SurveyModel(0,'','','');
  questionNo:number=0;
  dynamicNames=[];
  currentSurveyId:number;
  radioChecked=[];
  userSubmissionResult:any;
  submissionNumber:number;
  percentage:number=0;
  surveyTab:number;
  selectedRangeValue:number=0;
  @ViewChild('f') surveyQuestionForm:NgForm;
  @ViewChild('tabs') TabsPage

  currentFildValid:boolean=false;
  selectedArray=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public surveyService:SurveyserviceProvider,public globalVar:SharedGlobalProvider
    ,private storage: Storage) {
  }

  ionViewWillEnter() {
    this.surveyTab=this.globalVar.getSurveyOpened();
    this.questionNo=0;
    this.percentage=0;
    this.currentSurveyId=0;
    let loader=this.globalVar.presentLoading();
    this.surveyService.getSurveyDetail(this.globalVar.getUserId()).subscribe(
        (response:any[])=>{
          this.surveyQuestion=[];
          this.currentSurveyId=response['id'];
          this.surveyItemDetail=new SurveyModel(response['id'],response['name'],
          response['created_at'],response['updated_at']);
          if(response['questions']){
            this.submissionNumber=response['submission_no'];
            for(let question of response['questions']){
              if(question['question']['type']=='sc'){
                this.dynamicNames[question['question']['id']]=question['question']['options'][0]['value'];
                this.radioChecked[question['question']['id']]=question['question']['options'][0]['value'];
              }
              this.surveyQuestion.push(question['question']);
            }
          }
          loader.dismiss();
        },
        (error:Error)=>{
          if(error['error'][0]=='token_expired'){
            this.storage.set('user-token', '');
            this.storage.set('user-id', '');
            this.globalVar.setNotifyMessage('Sorry, your login has expired',2000,'error-toastr');
           }else{
            this.storage.set('user-id', '');
            this.storage.set('user-token', '');
            this.globalVar.setNotifyMessage('An unknown error occurred, please try again.',2000,'error-toastr');
          }
          loader.dismiss();
          this.navCtrl.push(LoginPage);
        }
      );


  }

  logForm(form:NgForm){
    this.nextField(this.questionNo+1,1);
  }
  previousField(index:number){
    this.questionNo=index;
    const formValues=this.surveyQuestionForm.value;
    if(this.surveyQuestion[index].type=='r'){
      this.selectedRangeValue=formValues['answer_'+this.surveyQuestion[index].id];
    }
    this.currentFildValid=true;
  }
  currentFieldValid(ev){
    this.currentFildValid=false;
    if(ev.value){
      this.currentFildValid=true;      
    }
  }

  currentFieldChecked(ev){
    if (ev.checked == true) {
      this.selectedArray.push(ev);
    } else {
     let newArray = this.selectedArray.filter(function(el) {
       return el.id !== ev.id;
    });
      this.selectedArray = newArray;
   }
   this.currentFildValid=true;
   if(this.selectedArray.length==0){
    this.currentFildValid=false;
   }
    
  }

  loadSumissionList(){
    if(this.globalVar.getUserId()){
      this.surveyService.getuserSurveys(this.globalVar.getUserId()).subscribe(
        (response:any[])=>{
          if(response){
              this.userSubmissionResult=response;
            }
          }
      );
    }
    return this.userSubmissionResult;
  }

  onRadioOption(qid:number,value:string){
    this.dynamicNames[qid]=value;
    this.radioChecked[qid]=value;
  }

  nextField(index:number,lastQuestion:number=0){
    const queArr=this.surveyQuestion[index-1];
    const formValues=this.surveyQuestionForm.value;

    let submittedArr=[];
    if(queArr.type!='mc'){
      submittedArr.push({'qid':queArr.id,'value':formValues['answer_'+queArr.id],'submission_no':this.submissionNumber});

    }else{
      const queArr=this.surveyQuestion[index-1];
      const multipleQuestionsArr=this.getMultipleAnswers(queArr);
      submittedArr.push({'qid':queArr.id,'value':multipleQuestionsArr,'submission_no':this.submissionNumber});
    }

    this.submitSigleAnswer(index,submittedArr,lastQuestion);
  }

  getMultipleAnswers(questionOpt,type:string='empty'){
    const formValues=this.surveyQuestionForm.value;
    const multipleQuestionsArr=[];
    const onlyTrueAnsArr=[];
    if(questionOpt.options.length>0){
      for(var i=0;i<questionOpt.options.length;i++){
        if(formValues['answer_'+questionOpt.id+'_'+i]){
          onlyTrueAnsArr.push(questionOpt.options[i]);
        }
        multipleQuestionsArr.push(formValues['answer_'+questionOpt.id+'_'+i]);
        
      }
    }

    return (type=='not-empty')?onlyTrueAnsArr:multipleQuestionsArr;
  }

  submitSigleAnswer(index,formValues,lastQuestion){

    let loader=this.globalVar.presentLoading();
    this.surveyService.postSurvey(formValues,this.currentSurveyId,this.globalVar.getUserId()).subscribe(
      (response:JSON)=>{
        this.percentage=response['percentage'];
        const formValues=this.surveyQuestionForm.value;
        if(lastQuestion==0){
          this.questionNo=index;
          this.selectedRangeValue=0;
          this.selectedArray=[];

          if(this.surveyQuestion[index].type=='sc' || this.surveyQuestion[index].type=='r'
          || this.surveyQuestion[index].type=='cq'){
            if(this.surveyQuestion[index].type=='r'){
              console.log(formValues['answer_'+this.surveyQuestion[index].id]);
              this.selectedRangeValue=formValues['answer_'+this.surveyQuestion[index].id];
            }
            this.currentFildValid=true;
          }else if(this.surveyQuestion[index].type=='t' && formValues['answer_'+this.surveyQuestion[index].id]){
            this.currentFildValid=true
          }else if(this.surveyQuestion[index].type=='mc'){
            const trueAnsArr=this.getMultipleAnswers(this.surveyQuestion[index],'not-empty');
            this.selectedArray=trueAnsArr;
            this.currentFildValid=(trueAnsArr.length>0)?true:false;
          }else{
            this.currentFildValid=false;
          }
        }else{
          this.surveyQuestionForm.reset();
          this.globalVar.setNotifyMessage(response['message'],2000,'success-toastr');
          this.navCtrl.push(ThankyouPage);
        }
        loader.dismiss();
      }
    );
  }

  openPage(pagetype:string){
    switch(pagetype){
      case 'new':
        this.navCtrl.push(SurveydetailPage);
        this.globalVar.setSurveyOpened(0);
      break;
      case 'old':
        this.navCtrl.push(SubmissionlistPage);
        this.globalVar.setSurveyOpened(1);
        this.navCtrl.parent.select(1);
      break;
    }

  }

  getRangeValue(ev){
    this.selectedRangeValue=ev.value;
  }
}
