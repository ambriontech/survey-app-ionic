import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubmissionlistPage } from './submissionlist';

@NgModule({
  declarations: [
    SubmissionlistPage,
  ],
  imports: [
    IonicPageModule.forChild(SubmissionlistPage),
  ],
})
export class SubmissionlistPageModule {}
