import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { SharedGlobalProvider } from '../../providers/shared-global/shared-global';
import { TabsPage } from '../tabs/tabs';
import { SurveydetailPage } from '../surveydetail/surveydetail';

/**
 * Generated class for the RegisterationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registeration',
  templateUrl: 'registeration.html',
})
export class RegisterationPage {
  private signupForm:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private formBuilder: FormBuilder,private userService:UserserviceProvider,
    public globalVar:SharedGlobalProvider) {
      this.signupForm=this.formBuilder.group({
        name:['',Validators.required],
        email:['',Validators.required],
        password:['',Validators.required]
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterationPage');
  }

  logForm() {
    console.log(this.signupForm.value);
    let loading=this.globalVar.presentLoading();
    this.userService.registerUser(this.signupForm.value).subscribe(
      (response:JSON)=>{
        if(response['status']==false){
          this.globalVar.setNotifyMessage(response['message'],3000,'error-toastr');
        }else{
          this.globalVar.setNotifyMessage(response['message'],3000,'success-toastr');
          this.autologin(this.signupForm.value);
        }
        loading.dismiss();
      },
      (error:Error)=>{
        this.globalVar.setNotifyMessage('An unknown error occurred, please try again.',3000,'error-toastr');
        loading.dismiss();
      }
    );
  }

  autologin(userArr:any){
    let loading=this.globalVar.presentLoading();
    this.userService.loginUser(userArr).subscribe(
      (response)=>{
          this.globalVar.setToken(response['token']);
          this.globalVar.setUserId(response['userId']);
          this.globalVar.setNotifyMessage('User logged in successfully',2000,'success-toastr');
          loading.dismiss();
          this.globalVar.setSurveyOpened(1);
          this.navCtrl.push(SurveydetailPage);
          this.navCtrl.setRoot(TabsPage);
          
      },
      (error:Error)=>{
        this.globalVar.setNotifyMessage('Please enter valid detail',2000,'error-toastr');
        loading.dismiss();
      }
    );
  }

}
