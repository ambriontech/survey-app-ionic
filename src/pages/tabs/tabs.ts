import { Component } from '@angular/core';
import { ChangePasswordPage } from '../change-password/change-password';
import { SubmissionlistPage } from '../submissionlist/submissionlist';
import { SurveydetailPage } from '../surveydetail/surveydetail';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = SurveydetailPage;
  tab2Root = SubmissionlistPage;
  tab3Root = ChangePasswordPage;

  constructor() {
  }

}
