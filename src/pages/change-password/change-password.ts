import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SharedGlobalProvider } from '../../providers/shared-global/shared-global';
import { LoginPage } from '../login/login';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {
  private changePasswordForm:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,
            private formBuilder:FormBuilder,private globalService:SharedGlobalProvider,
            private userService:UserserviceProvider,private app:App,private storage:Storage) {
              this.changePasswordForm=this.formBuilder.group({
                password:['',Validators.required],
                newPassword: ['', Validators.required],
                confirmPassword:   ['', Validators.required]
            });
  }

  ionViewWillEnter() {
    this.changePasswordForm.reset();
  }

  submitChangeForm(){
    
    let loading=this.globalService.presentLoading();
    this.userService.changePassword(this.changePasswordForm.value).subscribe(
      (response)=>{
        if(response['status']==true){
          this.globalService.setNotifyMessage(response['message'],2000,'success-toastr');
          this.navCtrl.push(LoginPage);
        }else{
          this.globalService.setNotifyMessage(response['message'],2000,'error-toastr');
        }
          
          loading.dismiss();
          
      },
      (error:Error)=>{
        this.globalService.setNotifyMessage('An unknown error occurred, please try again.',2000,'error-toastr');
        loading.dismiss();
      }
    );
  }

  logout(){
    this.globalService.setToken(null);
    var nav = this.app.getRootNav();
    this.storage.set('user-token', '');
    nav.setRoot(HomePage);
  }

}
