import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { SharedGlobalProvider } from '../../providers/shared-global/shared-global';
import { SurveyserviceProvider } from '../../providers/surveyservice/surveyservice';
import { SurveyModel } from '../../shared/survey.model';
import { NgForm } from '@angular/forms';
import { ThankyouPage } from '../thankyou/thankyou';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SurveyEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-survey-edit',
  templateUrl: 'survey-edit.html',
})
export class SurveyEditPage {
  surveyItemDetail:SurveyModel=new SurveyModel(0,'','','');
  dynamicNames=[];
  surveyQuestion=[];
  questionNo:number=0;
  answer=[];
  currentSurveyId:number;
  currentSurveyNo:number;
  radioChecked=[];
  percentage:number;
  @ViewChild('f') surveyQuestionForm:NgForm;
  currentFildValid:boolean=false;
  selectedArray=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private globalVar:SharedGlobalProvider,private surveyService:SurveyserviceProvider,
    private storage: Storage) {
  }

  ionViewWillEnter() {
    const surveyId=this.navParams.get('id');
    const surveyNo=this.navParams.get('sno');
    const lastQuestion=this.navParams.get('last_question');
    this.percentage=this.navParams.get('percentage');
    this.currentSurveyId=surveyId;
    this.currentSurveyNo=surveyNo;
    this.questionNo=(lastQuestion)?lastQuestion:0;
    if(surveyId){
      let loader=this.globalVar.presentLoading();
      const userId=this.globalVar.getUserId();
      this.surveyService.editSurveyDetail(userId,surveyNo).subscribe(
        (response:any[])=>{
          if(response.length>0){
            this.surveyItemDetail=new SurveyModel(response[0]['survey_id'],response[0]['name'],
          response[0]['created_at'],response[0]['updated_at']);
            for(let submission of response){
                  if(submission['type']=='sc'){
                    this.dynamicNames[submission['id']]=(submission['answer'])?submission['answer']:submission.options[0].value;
                    this.radioChecked[submission['id']]=(submission['answer'])?submission['answer']:submission.options[0].value;
                  }
                  this.surveyQuestion.push(submission);
            }
            if(this.surveyQuestion[this.questionNo].type=='r' || this.surveyQuestion[this.questionNo].type=='sc'){
              this.currentFildValid=true;
            } 
          }
          
          loader.dismiss();
        },
        (error:Error)=>{
          this.globalVar.setNotifyMessage('An unknown error occurred, please try again.',2000,'error-toastr');
        }
      );
    }
  }

  logForm(form:NgForm){
    this.nextField(this.questionNo+1,1);
  }

  previousField(index:number){
    this.questionNo=index;
    this.currentFildValid=true;
  }

  currentFieldValid(ev){
    this.currentFildValid=false;
    if(ev.value){
      this.currentFildValid=true;      
    }
  }

  currentFieldChecked(ev){
    if (ev.checked == true) {
      this.selectedArray.push(ev);
    } else {
      
     let newArray = this.selectedArray.filter(function(el) {
       return el.id !== ev.id;
    });
      this.selectedArray = newArray;
   }
   this.currentFildValid=true;
   if(this.selectedArray.length==0){
    this.currentFildValid=false;
   }
    
  }

  onRadioOption(opt,qid:number,value:string){
    this.dynamicNames[qid]=value;
    this.radioChecked[qid]=value;
    
  }

  nextField(index:number,lastQuestion:number=0){
    const queArr=this.surveyQuestion[index-1];
    const formValues=this.surveyQuestionForm.value;
    let submittedArr=[];
    if(queArr.type!='mc'){
      submittedArr.push({'qid':queArr.id,'value':formValues['answer_'+queArr.id],'delete':1});
      
    }else{
      const multipleQuestionsArr=[];
      for(var i=0;i<queArr.options.length;i++){
        multipleQuestionsArr.push(formValues['answer_'+queArr.id+'_'+i]);
      }
      submittedArr.push({'qid':queArr.id,'value':multipleQuestionsArr,'delete':1});
    }
    this.submitSigleAnswer(index,submittedArr,lastQuestion);
  }

  submitSigleAnswer(index,formValues,lastQuestion){
    let loader=this.globalVar.presentLoading();
    this.surveyService.editPostSurvey(formValues,this.currentSurveyId,this.globalVar.getUserId(),this.currentSurveyNo).subscribe(
      (response:JSON)=>{
        this.percentage=response['percentage'];
        if(lastQuestion==0){
          this.questionNo=index;
          if(this.surveyQuestion[index].type=='mc'){
            this.selectedArray=[];
            for(let opt of this.surveyQuestion[index].options){
              if(opt.checked==true){
                this.selectedArray.push(opt);
              }
            }
              
          }
          
          this.currentFildValid=(this.surveyQuestion[index].answer!=null || this.surveyQuestion[index].type=='sc' || this.surveyQuestion[index].type=='r')?true:false;
        }else{
          this.globalVar.setNotifyMessage(response['message'],2000,'success-toastr');
          this.navCtrl.push(ThankyouPage);
          //this.navCtrl.parent.select(1);
        }
        loader.dismiss();
      },
      (error:Error)=>{
        if(error['error'][0]=='token_expired'){
          this.storage.set('user-token', '');
          this.storage.set('user-id', '');
          this.globalVar.setNotifyMessage('Sorry, your login has expired',2000,'error-toastr');
         }else{
          this.storage.set('user-id', '');
          this.storage.set('user-token', '');
          this.globalVar.setNotifyMessage('There is some error',2000,'error-toastr');
        }
        loader.dismiss();
        this.navCtrl.push(LoginPage);
      }
    );
  }

}
