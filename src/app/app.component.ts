import { Component, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { SharedGlobalProvider } from '../providers/shared-global/shared-global';
import { SurveydetailPage } from '../pages/surveydetail/surveydetail';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  @ViewChild('nav') navCtrl: NavController;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    private storage: Storage,
    private globalService:SharedGlobalProvider) {
    this.storage.get('user-id').then((val) => {
      if(val){
        this.globalService.setUserId(val);
      }
      
    });
  this.storage.get('user-token').then((val) => {
    if(val){
      this.globalService.setToken(val);
        this.globalService.setSurveyOpened(1);
        this.navCtrl.push(SurveydetailPage);
        this.navCtrl.setRoot(TabsPage);
    }else{
      this.rootPage=HomePage;
    }
    
  });
  }
}
